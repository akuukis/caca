import 'ol/ol.css';
import Feature from 'ol/Feature';
import OLMap from 'ol/Map';
import View from 'ol/View';
import GeoJSON from 'ol/format/GeoJSON';
import { Tile as TileLayer, Vector as VectorLayer } from 'ol/layer';
import { Heatmap as HeatmapLayer } from 'ol/layer';
import { OSM, Vector as VectorSource } from 'ol/source';
import { Style } from 'ol/style';
import Icon from 'ol/style/Icon';
import KML from 'ol/format/KML';
import Text from 'ol/style/Text';
import { IReportedUnit, IUnit } from './unit';
import { predict } from './predict';
import { myTransform } from './constants';
import moment = require('moment')

class UnitFeature extends Feature {
    getProperties() { return super.getProperties() as IReportedUnit & {text: string, offsetX: number, textAlign: string}}
}

const createStyle = (img: string, anchor = [0.1, 0.1], scale = .25): Style => {
    return new Style({
        image: new Icon({
            src: img,
            scale: scale,
            anchor: anchor,
            zIndex: 10,
        })
    })
}

const styleCache: Record<IUnit.Type | IUnit.Size | IUnit.Activity | 'textLeftSide' | 'textRightSide', Style> = {
    'air defense': createStyle('/img/type/air_defense.svg'),
    'anti-tank': createStyle('/img/type/anti_armor.svg'),
    'engineer': createStyle('/img/type/engineer.svg'),
    'hq': createStyle('/img/type/hq.svg'),
    'mechanised infantry': createStyle('/img/type/infantry.svg'),
    'medics': createStyle('/img/type/medical.svg'),
    'mortars': createStyle('/img/type/mortar.svg'),
    'recce': createStyle('/img/type/reconnaissance.svg'),
    'self-propelled artillery': createStyle('/img/type/artilery.svg'),
    'supply': createStyle('/img/type/supply.svg'),
    'tank': createStyle('/img/type/armor.svg'),

    'team': createStyle('/img/size/team.svg', [-1.1, 0.8]),
    'squad': createStyle('/img/size/squad.svg', [-1.1, 0.7]),
    'platoon': createStyle('/img/size/platoon.svg', [0.1, 0.22]),
    'company': createStyle('/img/size/company.svg', [-1.1, 0.7]),
    'battalion': createStyle('/img/size/battalion.svg', [-1.1, 0.7]),

    'moving N': createStyle('/img/directions/n.svg', [-0.44, -1.1], .2),
    'moving NE': createStyle('/img/directions/ne.svg', [-0.5, -1.1], .2),
    'moving E': createStyle('/img/directions/e.svg', [-0.5, -1.1], .2),
    'moving SE': createStyle('/img/directions/se.svg', [-0.5, -1.1], .2),
    'moving S': createStyle('/img/directions/s.svg', [-0.44, -1.1], .2),
    'moving SW': createStyle('/img/directions/sw.svg', [-0.2, -1.1], .2),
    'moving W': createStyle('/img/directions/w.svg', [-0.1, -1.1], .2),
    'moving NW': createStyle('/img/directions/nw.svg', [-0.2, -1.1], .2),
    'stationary': new Style(),
    'stationary in cover': new Style(),

    'textLeftSide': new Style(),  // TODO
    'textRightSide': new Style(),  // TODO
};

const styleFunction = function(feature: UnitFeature, resolution: number) {
    const {type, text, offsetX, textAlign} = feature.getProperties()
    const style = styleCache[type];
    if(text) style.setText(new Text({
      offsetX: offsetX,
      text: text,
      textAlign: textAlign
    }))
    return style
};


const parseMilitaryTime = (timestamp: number): string => {
    if(!timestamp || timestamp === 0) return ''
    console.log(timestamp)

    const timezone = String.fromCodePoint('A'.codePointAt(0) + Number(moment.utc(timestamp).format('ZZ').slice(1,3)) - 1).replace('@', 'Z')

    return moment.utc(timestamp).format('DDhhmm_MMMYY').replace('_', timezone).toUpperCase()
}

const unitToGeoJsons = (unit: IReportedUnit): GeoJSON.Feature[] => [
    {
        type: 'Feature',
        properties: {type: unit.type},
        geometry: {
            type: 'Point',
            coordinates: myTransform(unit.location),
        }
    },
    {
        type: 'Feature',
        properties: {type: unit.size},
        geometry: {
            type: 'Point',
            coordinates: myTransform(unit.location),
        }
    },
    {
        type: 'Feature',
        properties: {type: unit.activity},
        geometry: {
            type: 'Point',
            coordinates: myTransform(unit.location),
        }
    },
    {
        type: 'Feature',
        properties: {
          type: 'textLeftSide',
          text: parseMilitaryTime(unit.timestamp * 1000),
          offsetX: -5,
          textAlign: 'right'
        },
        geometry: {
            type: 'Point',
            coordinates: myTransform(unit.location),
        }
    },
    {
        type: 'Feature',
        properties: {
          type: 'textRightSide',
          text: unit.additional,
          offsetX: 35,
          textAlign: 'left'
        },
        geometry: {
            type: 'Point',
            coordinates: myTransform(unit.location),
        }
    },
]


const reportedUnitsLayer = new VectorLayer({
    source: new VectorSource(),
    style: styleFunction
})

// const ghostUnitLayers = new Map(ghostUnitTemplateAsArray.map(([title, template]) => [
//     title,
//     new VectorLayer({
//         source: new VectorSource(),
//         style: styleFunction,
//         opacity: 0.5,
//     }),
// ]))
let ghostUnitSource = new VectorSource()
console.log("hello ", ghostUnitSource)
const ghostUnitLayer = new VectorLayer({
    source: ghostUnitSource,
    style: styleFunction,
    opacity: .5,
    zIndex: 2,
})

var heatmapLayer = new HeatmapLayer({
  source: new VectorSource(),
  blur: 100,
  opacity: .25,
  radius: 6,
  zIndex: 1,
  gradient: ['#008DA6', '#BF2600'],
  weight: function(feature) {
    return 6
  }
});


const map = new OLMap({
    layers: [
        new TileLayer({
            source: new OSM()
        }),
        reportedUnitsLayer,
        ghostUnitLayer,
        heatmapLayer
    ],
    target: 'map',
    view: new View({
        center: myTransform([
            23.96258610,
            56.73121279,
        ]),
        // extent: myTransform([
        //     23.96258610,
        //     56.73121279,
        //     24.26399164,
        //     56.84607353
        // ]),
        zoom: 12
    })
});

map.on('moveend', function(e) {
  var zoom = map.getView().getZoom();
  heatmapLayer.setRadius(zoom*2)
//  heatmapLayer.setBlur(zoom)
})
console.log('MAP', map.getLayers())

const testcase1: IReportedUnit[] = [
    {
        "location": [
            23.97211134546,
            56.801679423522
        ],
        "type": "recce",
        "size": "platoon",
        "activity": "moving NE",
        "alignment": "foe",
        "additional": null,
        "timestamp": 1580566611
    },
    {
        "location": [
            23.935740332946,
            56.754429527749
        ],
        "type": "tank",
        "size": "platoon",
        "activity": "moving NE",
        "alignment": "foe",
        "additional": "T-72",
        "timestamp": 1580566611
    },
    {
        "location": [
            23.949776106619,
            56.774021512484
        ],
        "type": "mechanised infantry",
        "size": "company",
        "activity": "moving NE",
        "alignment": "foe",
        "additional": null,
        "timestamp": 1580566611
    },
    {
        "location": [
            23.885740332946,
            56.765429527749
        ],
        "type": "tank",
        "size": "platoon",
        "activity": "moving E",
        "alignment": "foe",
        "additional": "T-72",
        "timestamp": 1580566611
    }
]
let counter = 2

const main = async () => {

    try {

        // const reportedUnits = await (await fetch('http://142.93.226.94:8888/points')).json() as IReportedUnit[]
        // const reportedUnits = await (await fetch('http://142.93.226.94:8888/demo/1')).json() as IReportedUnit[]
        const reportedUnits = testcase1.slice(0, counter++)
        console.log('raw', reportedUnits)

        const reportedUnitsGeojson = {
            type: "FeatureCollection",
            name: "reported units",
            features: reportedUnits.map(unitToGeoJsons).reduce((flat, curr) => flat.concat(curr), [])
        }
        console.log('geojson', reportedUnitsGeojson)

        reportedUnitsLayer.getSource().clear()
        reportedUnitsLayer.getSource().addFeatures((new GeoJSON()).readFeatures(reportedUnitsGeojson));

        ghostUnitLayer.getSource().clear()
        const templateUnitsGeojson = {
            type: "FeatureCollection",
            name: "reported units",
            features: predict(reportedUnits).map(unitToGeoJsons).reduce((flat, curr) => flat.concat(curr), [])
        }
        ghostUnitLayer.getSource().addFeatures((new GeoJSON()).readFeatures(templateUnitsGeojson));

        heatmapLayer.getSource().clear()
        heatmapLayer.getSource().addFeatures((new GeoJSON()).readFeatures(templateUnitsGeojson));

    } catch(err) {
        console.error(err)
    }

}

setInterval(main, 3000)
main()
